<body>
    <div class="container">
        <div class="row">
            <img src="<?= base_url();?>assets/frontend/img/logo.png" alt="Toya" width="200" style="margin: auto; padding-top: 20px; cursor:pointer" onclick="window.location.href='<?= base_url();?>'">
        </div>
        <div class="row" style="border-bottom: 1px solid black; width: 100%; margin: 0 auto;">
            &nbsp;
        </div>
        <br>
        <div class="row">
            <ul style="list-style: none; font-weight: 600;">
                <li style="float: left; margin-right: 50px; margin-left: 325px;"><a style="color:black" href="<?= base_url('user/productsale');?>">SALE</a></li>
                <li style="float: left; margin-right: 50px;"><a style="color:black" href="<?= base_url('user/category_product/1');?>">MEN'S</a></li>
                <li style="float: left; margin-right: 50px;"><a style="color:black" href="<?= base_url('user/category_product/2');?>">WOMEN'S</a></li>
                <li style="float: left; margin-right: 50px;"><a style="color:black" href="<?= base_url('user/contact');?>">CONTACT US</a></li>
            </ul>
        </div>
        
        <div class="clearfix"></div>
        <!-- HEADER END -->

        <!-- BODY START -->
        <div class="row">
          <div class="col-md-12 text-center">
            <br>
            <span style="font-weight: 500; font-size:30px"> CONTACT US
          </div>
        </div>

        <section>
          <br>
          <br>
            
            
                    <p>Working Hours</p>
                    <p>Monday-Friday: 09:00 - 17:00 (GMT+7)</p>
                    <br>
                    <p>Whatsapp</p>
                    <p>+62 818 08 55 3946</p>
                    <br>
                    <p>Email</p>
                    <p>info@toyabatik.com</p>


      </section>
      <div class="row">
        <div class="col-md-6">
          &nbsp;
        </div>
        <div class="col-md-6">
            
        </div>
      </div>
          
        <!-- BODY END -->

          <!-- Footer Start -->
          <div class="row" style="border-bottom: 1px solid black; width: 100%; margin: 0 auto;">
            &nbsp;
          </div>

          <section>
            <br>
            <div class="row">
              <div class="col-md-12" style="text-align: center;">
              <span style="padding: 0; margin: 0; font-family: 'Roboto', sans-serif; font-weight: 500; font-size: 12pt;">CUSTOMER SERVICE</span><br>
              <span style="padding: 0; margin: 0; font-family: 'Roboto', sans-serif; font-weight: 100; font-size: 9pt;"><a style="color:black;" href="<?= base_url('how-to-shop');?>">How to Shop</a></span><br>
                <span style="padding: 0; margin: 0; font-family: 'Roboto', sans-serif; font-weight: 100; font-size: 9pt;"><a style="color:black;" href="<?= base_url('return-exchange-policy');?>">Return & Exchange Policy</a></span><br>
                <span style="padding: 0; margin: 0; font-family: 'Roboto', sans-serif; font-weight: 100; font-size: 9pt;"><a style="color:black;" href="<?= base_url('shipping-policy');?>">Shipping Policy</a></span>
              </div>
            </div>
          </section>

    </div>