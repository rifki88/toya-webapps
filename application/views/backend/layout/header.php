<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title> :: Toya :: </title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url();?>assets/backend/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= base_url();?>assets/backend/css/sb-admin-2.min.css" rel="stylesheet">

  <link href="<?= base_url();?>assets/backend/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.6.2/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
  

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3"> TOYA <sup>2</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('admin/dashboard');?>a">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <!--div class="sidebar-heading">
        Interface
      </div-->

      <!-- Nav Item - Pages Collapse Menu -->
      <!--li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Components</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Components:</h6>
            <a class="collapse-item" href="<?//= base_url('admin/dashboard');?>">Buttons</a>
            <a class="collapse-item" href="<?//= base_url('admin/dashboard');?>">Cards</a>
          </div>
        </div>
      </li-->

      <!-- Nav Item - Utilities Collapse Menu -->
      <!--li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Utilities</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Utilities:</h6>
            <a class="collapse-item" href="<?//= base_url('admin/dashboard');?>">Colors</a>
            <a class="collapse-item" href="<?//= base_url('admin/dashboard');?>">Borders</a>
            <a class="collapse-item" href="<?//= base_url('admin/dashboard');?>">Animations</a>
            <a class="collapse-item" href="<?//= base_url('admin/dashboard');?>">Other</a>
          </div>
        </div>
      </li-->

      <!-- Divider -->
      <!--hr class="sidebar-divider"-->

      <!-- Heading -->
      <!--div class="sidebar-heading">
        Addons
      </div-->

      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('admin/slider');?>">
          <i class="fas fa-fw fa-camera"></i>
          <span>Slider</span></a>
      </li>


      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Front Display</span>
        </a>
        <div id="collapsePages" class="collapse show" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"> Display : </h6>
            <a class="collapse-item" href="<?= base_url('admin/left');?>">Left Display</a>
            <a class="collapse-item" href="<?= base_url('admin/top');?>">Upper Display </a>
            <a class="collapse-item" href="<?= base_url('admin/buttom');?>">Buttom Display </a>
            <!--div class="collapse-divider"></div>
            <h6 class="collapse-header">Other Pages:</h6>
            <a class="collapse-item" href="<?//= base_url('admin/dashboard');?>">404 Page</a>
            <a class="collapse-item active" href="<?//= base_url('admin/dashboard');?>">Blank Page</a-->
          </div>
        </div>
      </li>

      <!-- Nav Item - Charts -->
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('admin/products');?>">
          <i class="fas fa-fw fa-industry"></i>
          <span>Products</span></a>
      </li>

      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('admin/setting');?>">
          <i class="fas fa-fw fa-table"></i>
          <span>Setting</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">